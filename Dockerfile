FROM python:3.10-bullseye

WORKDIR /app

COPY . /requirements.txt

RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

COPY . /app

USER admin

CMD /wait && uvicorn main:app --reload --host 0.0.0.0

# CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]


# FROM tiangolo/uvicorn-gunicorn-fastapi:python3.9

# COPY ./requirements.txt /app/requirements.txt

# RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt
