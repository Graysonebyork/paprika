fastapi>=0.68.0,<0.69.0
pydantic>=1.8.0,<2.0.0
uvicorn>=0.15.0,<0.16.0s
psycopg[binary,pool]>=3.1.2

#sqlalchemy>=2.0.0
