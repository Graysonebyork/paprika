import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/

// export default defineConfig({
//   plugins: [react()],
// })


export default defineConfig({
  plugins: [react({ include: '**/*.jsx' })],
  server: {
      hmr: true,
      host: true,
      strictPort: true,
      watch: {
          usePolling: true,
      },
  },
})
